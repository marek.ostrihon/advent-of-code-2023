import re

THRESHOLDS = {
    'red': 12,
    'green': 13,
    'blue': 14,
}

with open('input.txt') as file:
    total_sum = 0
    for row in file:
        game_is_possible = True
        game_numbers = re.sub(r"\bGame\s\d+:\s", "", row)
        steps = game_numbers.split(';')
        for step in steps:
            cube_set = step.split(',')

            cube_dict = {}
            for cube in cube_set:
                cube = cube.strip()
                number, color = cube.split()
                number = int(number)
                if number > THRESHOLDS[color]:
                    game_is_possible = False
                    break

            if game_is_possible is False:
                break

        if game_is_possible:
            # Get game ID and add
            game_info = re.findall(r"\bGame\s\d+:\s", row)[0]
            game_id = re.findall(r'\d+', game_info)[0]
            total_sum += int(game_id)

    print(f'The total_sum is {total_sum}')
