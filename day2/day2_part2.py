import re

RED = 'red'
GREEN = 'green'
BLUE = 'blue'

THRESHOLDS = {
    RED: 12,
    GREEN: 13,
    BLUE: 14,
}

with open('input.txt') as file:
    total_sum = 0
    for row in file:
        game_numbers = re.sub(r"\bGame\s\d+:\s", "", row)
        steps = game_numbers.split(';')

        cube_dict = {
            RED: [],
            GREEN: [],
            BLUE: [],
        }
        for step in steps:
            cube_set = step.split(',')


            for cube in cube_set:
                cube = cube.strip()
                number, color = cube.split()

                cube_dict[color].append(int(number))

        max_red = max(cube_dict[RED])
        max_green = max(cube_dict[GREEN])
        max_blue = max(cube_dict[BLUE])

        product = max_red * max_green * max_blue

        total_sum += product

    print(f'The total_sum is {total_sum}')
