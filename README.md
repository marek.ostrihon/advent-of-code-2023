# Advent of Code 2023 Solutions

[Advent of Code 2023](https://adventofcode.com/2023/)

This repository contains my solutions to the Advent of Code 2023 
challenges. Advent of Code is an annual coding event that takes
place in December, featuring daily programming puzzles. For more
information and to participate, visit 
[Advent of Code 2023](https://adventofcode.com/2023/).

## Table of Contents

- [Introduction](#introduction)
- [Folder Structure](#folder-structure)
- [Getting Started](#getting-started)
- [Usage](#usage)

## Introduction

Each day's challenge is presented as a two-part puzzle, 
with the second part unlocking after completing the first. 
Solutions are implemented in Python programming language
(it is possible that I will use JS for some day
it depends on how lazy I'll be),
and each day has its dedicated folder.

Feel free to explore the solutions, provide feedback,
or compare approaches. Remember not to spoil the fun 
for others who are still working on the challenges!

## Folder Structure

```
advent_of_code
│   README.md
│   .gitignore
│
└───day1
│   │   day1.py
│   │   input.txt
│   
└───day2
    │   day2_part1.txt
    │   day2_part2.txt
    │   input.txt
...
```

## Getting Started

To get started, clone this repository:

```bash
git clone https://gitlab.com/marek.ostrihon/advent-of-code-2023.git
cd day1
python day1.py
```