with (open('input.txt') as file):
    total_sum = 0
    lines = file.readlines()
    number_of_lines = len(lines)
    for line_index, line in enumerate(lines):
        previous_line = lines[line_index - 1] if line_index > 0 else None
        next_line = lines[line_index + 1] if line_index < number_of_lines - 1 else None

        number = ''
        within_number = False
        touches_special_character = False
        for char_index, char in enumerate(line):
            if char.isnumeric():
                within_number = True
                number += char
                if not touches_special_character:
                    # Check surroundings
                    surrounding_chars = []
                    try:
                        surrounding_chars.append(line[char_index - 1])
                    except IndexError:
                        pass
                    try:
                        surrounding_chars.append(line[char_index + 1])
                    except IndexError:
                        pass
                    if previous_line:
                        try:
                            surrounding_chars.append(previous_line[char_index - 1])
                        except IndexError:
                            pass
                        try:
                            surrounding_chars.append(previous_line[char_index])
                        except IndexError:
                            pass
                        try:
                            surrounding_chars.append(previous_line[char_index + 1])
                        except IndexError:
                            pass
                    if next_line:
                        try:
                            surrounding_chars.append(next_line[char_index - 1])
                        except IndexError:
                            pass
                        try:
                            surrounding_chars.append(next_line[char_index])
                        except IndexError:
                            pass
                        try:
                            surrounding_chars.append(next_line[char_index + 1])
                        except IndexError:
                            pass
                    if any(map(lambda x: not x.isnumeric() and x != '.'and x != '\n', surrounding_chars)):
                        touches_special_character = True

            elif (not char.isnumeric() and within_number) or (char.isnumeric() and char_index == len(line) - 1):
                if touches_special_character:
                    total_sum += int(number)
                number = ''
                within_number = False
                touches_special_character = False

print(f'Total sum is {total_sum}')
