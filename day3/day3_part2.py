with (open('input.txt') as file):
    total_sum = 0
    lines = file.readlines()
    number_of_lines = len(lines)
    surrounding_asterisks = []
    touchy_asterisks = {}
    for line_index, line in enumerate(lines):
        previous_line_index = line_index - 1
        next_line_index = line_index + 1

        previous_line = lines[previous_line_index] if line_index > 0 else None
        next_line = lines[next_line_index] if line_index < number_of_lines - 1 else None

        number = ''
        within_number = False
        for char_index, char in enumerate(line):
            if char.isnumeric():
                within_number = True
                number += char

                # Check surroundings for asterisks
                try:
                    if char := line[char_index - 1] == '*':
                        surrounding_asterisks.append(f'{line_index}_{char_index - 1}')
                except IndexError:
                    pass
                try:
                    if char := line[char_index + 1] == '*':
                        surrounding_asterisks.append(f'{line_index}_{char_index + 1}')
                except IndexError:
                    pass
                if previous_line:
                    try:
                        if char := previous_line[char_index - 1] == '*':
                            surrounding_asterisks.append(f'{previous_line_index}_{char_index - 1}')
                    except IndexError:
                        pass
                    try:
                        if char := previous_line[char_index] == '*':
                            surrounding_asterisks.append(f'{previous_line_index}_{char_index}')
                    except IndexError:
                        pass
                    try:
                        if char := previous_line[char_index + 1] == '*':
                            surrounding_asterisks.append(f'{previous_line_index}_{char_index + 1}')
                    except IndexError:
                        pass
                if next_line:
                    try:
                        if char := next_line[char_index - 1] == '*':
                            surrounding_asterisks.append(f'{next_line_index}_{char_index - 1}')
                    except IndexError:
                        pass
                    try:
                        if char := next_line[char_index] == '*':
                            surrounding_asterisks.append(f'{next_line_index}_{char_index}')
                    except IndexError:
                        pass
                    try:
                        if char := next_line[char_index + 1] == '*':
                            surrounding_asterisks.append(f'{next_line_index}_{char_index + 1}')
                    except IndexError:
                        pass

            elif (not char.isnumeric() and within_number) or (char.isnumeric() and char_index == len(line) - 1):
                if surrounding_asterisks:
                    surrounding_asterisks = set(surrounding_asterisks)
                    for surrounding_asterisk in surrounding_asterisks:
                        touchy_asterisks.setdefault(surrounding_asterisk, []).append(number)
                    surrounding_asterisks = []
                number = ''
                within_number = False

    for touchy_asterisk, numbers in touchy_asterisks.items():
        if len(numbers) == 2:
            first, second = numbers
            product = int(first) * int(second)
            total_sum += product

print(f'Total sum is {total_sum}')
