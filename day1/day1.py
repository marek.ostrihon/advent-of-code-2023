valid_strings = [
    'one', '1',
    'two', '2',
    'three', '3',
    'four', '4',
    'five', '5',
    'six', '6',
    'seven', '7',
    'eight', '8',
    'nine', '9'
]

numerals_as_words = {
    'one': '1',
    'two': '2',
    'three': '3',
    'four': '4',
    'five': '5',
    'six': '6',
    'seven': '7',
    'eight': '8',
    'nine': '9',
}

with open('input.txt') as file:
        total_sum = 0
        for row in file:
            # Find indexes of valid substrings
            indexes_of_valid_strings = {}
            for valid_string in valid_strings:
                if valid_string not in row:
                    continue
                low_index = row.find(valid_string)
                high_index = row.rfind(valid_string)
                if low_index == high_index:
                    indexes_of_valid_strings[low_index] = valid_string
                else:
                    indexes_of_valid_strings[low_index] = valid_string
                    indexes_of_valid_strings[high_index] = valid_string
            # Find lowest and highest index
            lowest_index = min(indexes_of_valid_strings.keys())
            highest_index = max((indexes_of_valid_strings.keys()))

            lowest_numeral = indexes_of_valid_strings[lowest_index]
            highest_numeral = indexes_of_valid_strings[highest_index]

            # Get numerals for spelled words
            if not lowest_numeral.isnumeric():
                lowest_numeral = numerals_as_words[lowest_numeral]
            if not highest_numeral.isnumeric():
                highest_numeral = numerals_as_words[highest_numeral]

            first = lowest_numeral
            last = highest_numeral
            together = first + last
            print(first, '&' , last)
            print(together)
            total_sum += int(together)
            print(f'prozatimni total_sum je {total_sum}')
        print(f'the total_sum is {total_sum}')
