import re
import time
from collections import deque
from itertools import count


class Card:

    def __init__(self, line):
        match = re.match(r"\bCard\s+\d+:\s", line)
        card_id = match[0].lstrip('Card ').rstrip(': ')
        self.id = int(card_id)

        clean_line = re.sub(r"\bCard\s+\d+:\s", "", line)

        _winning_numbers, _my_numbers = clean_line.split('|')

        self.winning_numbers = _winning_numbers.split()
        self.my_numbers = _my_numbers.split()

    @property
    def amount_of_matched_numbers(self):
        amount_of_matched_numbers = 0
        for winning_number in self.winning_numbers:
            if winning_number in self.my_numbers:
                amount_of_matched_numbers += 1
        return amount_of_matched_numbers


class MyPocket(deque):
    def __init__(self):
        super().__init__()
        self.total_cards_through_my_pocket = 0

    def append(self, __object):
        self.total_cards_through_my_pocket += 1
        return super().append(__object)

with (open('input.txt') as file):
    total_sum = 0
    cards = {}
    my_pocket = MyPocket()
    for line in file:
        card = Card(line)
        cards[card.id] = card

        my_pocket.append(card)

    start = time.time()
    graph_data = {}
    counter = count(1)
    while len(my_pocket) > 0:
        actual_card = my_pocket.popleft()
        for i in range(1, actual_card.amount_of_matched_numbers + 1):
            my_pocket.append(
                cards[actual_card.id + int(i)]
            )
        print(len(my_pocket))
        now = time.time()
        if next(counter) % 1000 == 0:
            time_since_start = now - start
            graph_data[time_since_start] = len(my_pocket)

with open('graph_data_time.txt', 'w') as file:
    print('\n'.join(map(str,graph_data.keys())), file=file)
with open('graph_data_cards.txt', 'w') as file:
    print('\n'.join(map(str,graph_data.values())), file=file)
print('counter ', next(counter))
print(f'The total sum of cards is {my_pocket.total_cards_through_my_pocket}')