import re

with (open('input.txt') as file):
    total_sum = 0
    for line in file:
        clean_line = re.sub(r"\bCard\s+\d+:\s", "", line)

        winning_numbers, my_numbers = clean_line.split('|')

        winning_numbers = winning_numbers.split()
        my_numbers = my_numbers.split()

        amount_of_matched_numbers = 0
        for winning_number in winning_numbers:
            if winning_number in my_numbers:
                amount_of_matched_numbers += 1

        if amount_of_matched_numbers != 0:
            total_sum += 2 ** (amount_of_matched_numbers - 1)

print(f'The total_sum is {total_sum}')